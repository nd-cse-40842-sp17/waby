#!/bin/sh

USER=${USER:-$(whoami)}

curl	-F user=$USER \
	-F source=@${1} \
	xavier.h4x0r.space:9325
