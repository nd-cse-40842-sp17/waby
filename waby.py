#!/usr/bin/python2.7

import collections
import json
import logging
import os
import shlex
import socket
import sys
import time

import tornado.gen
import tornado.ioloop
import tornado.options
import tornado.web

# Configuration

WABY_ADDRESS = '0.0.0.0'
WABY_PORT    = 9325
WABY_SANDBOX = os.path.join(os.path.abspath(os.curdir), 'sandbox')
WABY_INPUT   = 'input.txt'
WABY_OUTPUT  = 'output.txt'
WABY_STATE   = 'state.json'
WABY_SCORES  = collections.defaultdict(dict)

try:
    WABY_SCORES.update(json.load(open(WABY_STATE)))
except (IOError, ValueError):
    pass

# Waby Handler

class WabyHandler(tornado.web.RequestHandler):

    def get(self):
        self.render('index.html', scores=WABY_SCORES)

    @tornado.gen.coroutine
    def post(self):
        user    = self.get_argument('user'     , None)
        source  = self.request.files['source'][0]
        sandbox = os.path.join(WABY_SANDBOX, user, str(int(time.time())))

        if not os.path.exists(sandbox):
            os.makedirs(sandbox)

        source_path = os.path.join(sandbox, source['filename'])
        with open(source_path, 'w') as fs:
            fs.write(source['body'])

        command = 'scripts/sandbox.sh {} scripts/run.py {} {} {}'.format(
            sandbox, source['filename'], WABY_INPUT, WABY_OUTPUT
        )

        process = tornado.process.Subprocess(shlex.split(command), stdout=tornado.process.Subprocess.STREAM)
        result  = yield tornado.gen.Task(process.stdout.read_until_close)
        data    = json.loads(result.decode('utf-8'))
        language= data['language']
        key     = '{}|{}'.format(user, language)

        data['lines']  = len(source['body'].splitlines())
        data['stdout'] = open(os.path.join(sandbox, 'stdout')).read()

        lineCountLess  = data['lines'] <= WABY_SCORES[key].get('lines', 1000000)
        successAndLess = data['status'] == 0 and (lineCountLess or WABY_SCORES[key].get('status', 1) != 0)
        failureAndLess = data['status'] != 0 and WABY_SCORES[key].get('status', 1) != 0 and lineCountLess

        if successAndLess or failureAndLess:
            WABY_SCORES[key]['source']    = source_path.replace(WABY_SANDBOX, '')
            WABY_SCORES[key]['status']    = data['status']
            WABY_SCORES[key]['result']    = data['result']
            WABY_SCORES[key]['lines']     = data['lines']
            WABY_SCORES[key]['language']  = data['language']
            WABY_SCORES[key]['timestamp'] = time.time()

        self.write(json.dumps(data))
        json.dump(WABY_SCORES, open(WABY_STATE, 'w'))

class SourceHandler(tornado.web.RequestHandler):

    def get(self, path):
        path = os.path.join(WABY_SANDBOX, path)
        if not os.path.exists(path):
            raise tornado.web.HTTPError(404, 'This is not the droid you are looking for')

        self.set_header('Content-Type', 'text/plain; charset="utf-8"')

        for line in open(path):
            self.write(line)

# Waby Application

class WabyApplication(tornado.web.Application):
    def __init__(self, **settings):
        tornado.web.Application.__init__(self, **settings)

        self.logger   = logging.getLogger()
        self.address  = settings.get('address', WABY_ADDRESS)
        self.port     = settings.get('port', WABY_PORT)
        self.ioloop   = tornado.ioloop.IOLoop.instance()

        self.add_handlers('', [
            (r'/'     , WabyHandler),
            (r'/(.*)' , SourceHandler),
        ])

    def run(self):
        try:
            self.listen(self.port, self.address)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {}:{} = {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Main Execution

if __name__ == '__main__':
    tornado.options.define('debug', default=False, help='Enable debugging mode.')
    tornado.options.define('port', default=WABY_PORT, help='Port to listen on.')
    tornado.options.define('template_path', default=os.path.join(os.path.dirname(__file__), "templates"), help='Path to templates')
    tornado.options.parse_command_line()

    options = tornado.options.options.as_dict()
    waby    = WabyApplication(**options)
    waby.run()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
