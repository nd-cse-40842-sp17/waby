FROM	    ubuntu
MAINTAINER  Peter Bui

RUN	    apt-get update -y
RUN	    apt-get install -y gcc g++ python ruby perl python-tornado haskell-platform wget default-jdk make
RUN	    mkdir -p /opt ; cd /opt ; wget https://repo1.maven.org/maven2/org/clojure/clojure/1.8.0/clojure-1.8.0.zip ; unzip clojure-1.8.0.zip
RUN	    mkdir -p /opt ; cd /opt ; wget  http://www.yabasic.de/download/yabasic-2.78.0.tar.gz ; tar xzvf yabasic-2.78.0.tar.gz ; cd yabasic-2.78.0 ; ./configure --prefix /opt/yabasic-2.78 ; make ; make install
